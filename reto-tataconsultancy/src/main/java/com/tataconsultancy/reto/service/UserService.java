package com.tataconsultancy.reto.service;

import com.tataconsultancy.reto.entity.User;
import com.tataconsultancy.reto.util.GenericService;

public interface UserService extends GenericService<User, Long> {

  User findByUsername(String username);

  User findByEmail(String username);

}
