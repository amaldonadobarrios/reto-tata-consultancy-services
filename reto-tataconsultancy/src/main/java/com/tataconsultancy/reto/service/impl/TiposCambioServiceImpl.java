package com.tataconsultancy.reto.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tataconsultancy.reto.beans.request.TiposCambioRequest;
import com.tataconsultancy.reto.beans.response.TiposCambioResponse;
import com.tataconsultancy.reto.entity.TiposCambio;
import com.tataconsultancy.reto.repository.MonedaRepository;
import com.tataconsultancy.reto.repository.OpcionTipoCambioRepository;
import com.tataconsultancy.reto.repository.TiposCambioRepository;
import com.tataconsultancy.reto.repository.UserRepository;
import com.tataconsultancy.reto.service.TiposCambioService;
import com.tataconsultancy.reto.util.exception.ResourceNotFoundException;
@Service
public class TiposCambioServiceImpl implements TiposCambioService {

	@Autowired
	TiposCambioRepository  tiposCambioRepository;
	
	@Autowired
	MonedaRepository monedaRepository;
	
	@Autowired
	OpcionTipoCambioRepository opcionTipoCambioRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Override
	public List<TiposCambioResponse> findAll() {
		List<TiposCambioResponse>  lista= new ArrayList<>();
		tiposCambioRepository.findAll().forEach(n-> lista.add(mapToTiposCambioResponse(n)));
		return lista;
	}

	@Override
	public TiposCambioResponse get(Long id) {
		return mapToTiposCambioResponse(tiposCambioRepository.findById(id).get());
	}

	@Override
	public TiposCambioResponse findByMoneda1AndMoneda2(long monedaOrigen, long monedaDestino) {
		TiposCambio tipoCambio=null;
		try {
			tipoCambio=tiposCambioRepository.findByMoneda1AndMoneda2(monedaRepository.getOne(monedaOrigen), monedaRepository.getOne(monedaDestino));
		} catch (Exception e) {
			throw new ResourceNotFoundException("NO SE ENCONTRO EL TIPO DE CAMBIO");
		}
		return mapToTiposCambioResponse(tipoCambio);
	}

	@Override
	public TiposCambioResponse save(TiposCambioRequest request) {
		return mapToTiposCambioResponse(tiposCambioRepository.save(mapTiposCambioRequestToEntity(request)));
	}
	
	@Override
	public TiposCambioResponse update(Long idTipoCambio, TiposCambioRequest request) {
		return mapToTiposCambioResponse(tiposCambioRepository.save(mapToTiposCambioResponse(tiposCambioRepository.findById(idTipoCambio).get(),request)));
	}
	
	private TiposCambio mapTiposCambioRequestToEntity(TiposCambioRequest request) {
		TiposCambio entity= new TiposCambio();
		entity.setFechaRegistro(new Date());
		entity.setOpcionTipoCambio(opcionTipoCambioRepository.getOne(Long.valueOf(request.getIdOpcionTipoCambio())));
		entity.setMoneda1(monedaRepository.getOne(Long.valueOf(request.getIdMonedaOrigen())));
		entity.setMoneda2(monedaRepository.getOne(Long.valueOf(request.getIdMonedaDestino())));
		entity.setUsuario(userRepository.getOne(Long.valueOf(request.getIdUsuario())));
		entity.setValorCambio(BigDecimal.valueOf(request.getValorCambio()));
		return entity;
	}

	private TiposCambioResponse mapToTiposCambioResponse(TiposCambio entity) {
		TiposCambioResponse response= new TiposCambioResponse();
		response.setFechaActualiza(entity.getFechaActualiza());
		response.setFechaRegistro(entity.getFechaRegistro());
		response.setIdTipoCambio(entity.getIdTipoCambio());
		response.setMonedaDestino(entity.getMoneda2().getNombreMoneda());
		response.setMonedaOrigen(entity.getMoneda1().getNombreMoneda());
		response.setOpcionTipoCambio(entity.getOpcionTipoCambio().getNombreTipoOpcion());
		response.setUsuarioActualiza(entity.getIdUsuarioActualiza()!=null?entity.getIdUsuarioActualiza().getLastname():"");
		response.setUsuarioRegistra(entity.getUsuario().getLastname());
		response.setValorCambio(entity.getValorCambio().doubleValue());
		return response;
	}

	private TiposCambio mapToTiposCambioResponse(TiposCambio entity,TiposCambioRequest request ) {
		entity.setFechaActualiza(new Date());
		entity.setOpcionTipoCambio(opcionTipoCambioRepository.getOne(Long.valueOf(request.getIdOpcionTipoCambio())));
		entity.setMoneda1(monedaRepository.getOne(Long.valueOf(request.getIdMonedaOrigen())));
		entity.setMoneda2(monedaRepository.getOne(Long.valueOf(request.getIdMonedaDestino())));
		entity.setIdUsuarioActualiza(userRepository.getOne(Long.valueOf(request.getIdUsuario())));
		entity.setValorCambio(BigDecimal.valueOf(request.getValorCambio()));
		return entity;
	}
}
