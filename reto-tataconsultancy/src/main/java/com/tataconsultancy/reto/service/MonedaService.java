package com.tataconsultancy.reto.service;

import java.util.List;

import com.tataconsultancy.reto.dto.MonedaDTO;


public interface MonedaService {

	List<MonedaDTO> findAll();

    MonedaDTO get(Long id);

}
