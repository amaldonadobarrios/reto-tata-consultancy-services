package com.tataconsultancy.reto.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tataconsultancy.reto.dto.MonedaDTO;
import com.tataconsultancy.reto.entity.Moneda;
import com.tataconsultancy.reto.repository.MonedaRepository;
import com.tataconsultancy.reto.service.MonedaService;

@Service
public class MonedaServiceImpl implements MonedaService {

	@Autowired
	MonedaRepository monedaRepository;

	@Override
	public List<MonedaDTO> findAll() {
		List<MonedaDTO> lista = new ArrayList<MonedaDTO>();
		monedaRepository.findAll().forEach(n -> lista.add(mapToDTO(n)));
		return lista;
	}

	@Override
	public MonedaDTO get(final Long id) {
		return mapToDTO(monedaRepository.findById(id).get());
	}

	private MonedaDTO mapToDTO(final Moneda moneda) {
		final MonedaDTO monedaResponse = new MonedaDTO();
		monedaResponse.setIdMoneda(moneda.getIdMoneda());
		monedaResponse.setNombreMoneda(moneda.getNombreMoneda());
		monedaResponse.setPaisMoneda(moneda.getPaisMoneda());
		monedaResponse.setSimboloMoneda(moneda.getSimboloMoneda());
		return monedaResponse;
	}

}
