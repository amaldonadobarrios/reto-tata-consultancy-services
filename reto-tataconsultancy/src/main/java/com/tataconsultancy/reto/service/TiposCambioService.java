package com.tataconsultancy.reto.service;

import java.util.List;

import com.tataconsultancy.reto.beans.request.TiposCambioRequest;
import com.tataconsultancy.reto.beans.response.TiposCambioResponse;


public interface TiposCambioService {

	List<TiposCambioResponse> findAll();

	TiposCambioResponse get(Long id);
	
	TiposCambioResponse findByMoneda1AndMoneda2 (long monedaOrigen, long monedaDestino);
	
	TiposCambioResponse save(TiposCambioRequest request);

	TiposCambioResponse update(Long idTipoCambio, TiposCambioRequest request);
}
