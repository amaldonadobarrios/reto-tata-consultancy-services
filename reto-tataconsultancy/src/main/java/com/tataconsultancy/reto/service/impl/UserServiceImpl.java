package com.tataconsultancy.reto.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tataconsultancy.reto.entity.User;
import com.tataconsultancy.reto.repository.UserRepository;
import com.tataconsultancy.reto.service.UserService;
import com.tataconsultancy.reto.util.AbstractServiceImpl;



@Service
public class UserServiceImpl extends AbstractServiceImpl<User, Long> implements UserService {
	
 @Autowired
  private UserRepository userRepository;


  @Transactional(readOnly = true )
  @Override
  public User findByUsername(String username) {
    return userRepository.findByUsername(username);
  }

  @Transactional(readOnly = true)
  @Override
  public User findByEmail(String username) {
    return userRepository.findByEmail(username);
  }

}
