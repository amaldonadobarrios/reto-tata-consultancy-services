package com.tataconsultancy.reto.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the TIPOS_CAMBIO database table.
 * 
 */
@Entity
@Table(name="TIPOS_CAMBIO")
@NamedQuery(name="TiposCambio.findAll", query="SELECT t FROM TiposCambio t")
public class TiposCambio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_TIPO_CAMBIO", unique=true, nullable=false)
	private long idTipoCambio;

	@Column(name="FECHA_ACTUALIZA")
	private Date fechaActualiza;

	@Column(name="FECHA_REGISTRO", nullable=false)
	private Date fechaRegistro;

	@ManyToOne
	@JoinColumn(name="ID_USUARIO_ACTUALIZA", nullable=true)
	private User idUsuarioActualiza;

	@Column(name="VALOR_CAMBIO", nullable=false, precision=5, scale=3)
	private BigDecimal valorCambio;

	//bi-directional many-to-one association to OperacionesTipoCambio
	@OneToMany(mappedBy="tiposCambio")
	private List<OperacionesTipoCambio> operacionesTipoCambios;

	//bi-directional many-to-one association to Moneda
	@ManyToOne
	@JoinColumn(name="ID_MONEDA_ORIGEN", nullable=false)
	private Moneda moneda1;

	//bi-directional many-to-one association to Moneda
	@ManyToOne
	@JoinColumn(name="ID_MONEDA_DESTINO", nullable=false)
	private Moneda moneda2;

	//bi-directional many-to-one association to OpcionTipoCambio
	@ManyToOne
	@JoinColumn(name="ID_OPCION_TIPO_CAMBIO", nullable=false)
	private OpcionTipoCambio opcionTipoCambio;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="ID_USUARIO_REGISTRA", nullable=false)
	private User usuario;

	public TiposCambio() {
	}

	public long getIdTipoCambio() {
		return this.idTipoCambio;
	}

	public void setIdTipoCambio(long idTipoCambio) {
		this.idTipoCambio = idTipoCambio;
	}

	public Date getFechaActualiza() {
		return this.fechaActualiza;
	}

	public void setFechaActualiza(Date fechaActualiza) {
		this.fechaActualiza = fechaActualiza;
	}

	public Date getFechaRegistro() {
		return this.fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public User getIdUsuarioActualiza() {
		return this.idUsuarioActualiza;
	}

	public void setIdUsuarioActualiza(User idUsuarioActualiza) {
		this.idUsuarioActualiza = idUsuarioActualiza;
	}

	public BigDecimal getValorCambio() {
		return this.valorCambio;
	}

	public void setValorCambio(BigDecimal valorCambio) {
		this.valorCambio = valorCambio;
	}

	public List<OperacionesTipoCambio> getOperacionesTipoCambios() {
		return this.operacionesTipoCambios;
	}

	public void setOperacionesTipoCambios(List<OperacionesTipoCambio> operacionesTipoCambios) {
		this.operacionesTipoCambios = operacionesTipoCambios;
	}

	public OperacionesTipoCambio addOperacionesTipoCambio(OperacionesTipoCambio operacionesTipoCambio) {
		getOperacionesTipoCambios().add(operacionesTipoCambio);
		operacionesTipoCambio.setTiposCambio(this);

		return operacionesTipoCambio;
	}

	public OperacionesTipoCambio removeOperacionesTipoCambio(OperacionesTipoCambio operacionesTipoCambio) {
		getOperacionesTipoCambios().remove(operacionesTipoCambio);
		operacionesTipoCambio.setTiposCambio(null);

		return operacionesTipoCambio;
	}

	public Moneda getMoneda1() {
		return this.moneda1;
	}

	public void setMoneda1(Moneda moneda1) {
		this.moneda1 = moneda1;
	}

	public Moneda getMoneda2() {
		return this.moneda2;
	}

	public void setMoneda2(Moneda moneda2) {
		this.moneda2 = moneda2;
	}

	public OpcionTipoCambio getOpcionTipoCambio() {
		return this.opcionTipoCambio;
	}

	public void setOpcionTipoCambio(OpcionTipoCambio opcionTipoCambio) {
		this.opcionTipoCambio = opcionTipoCambio;
	}

	public User getUsuario() {
		return this.usuario;
	}

	public void setUsuario(User usuario) {
		this.usuario = usuario;
	}

}