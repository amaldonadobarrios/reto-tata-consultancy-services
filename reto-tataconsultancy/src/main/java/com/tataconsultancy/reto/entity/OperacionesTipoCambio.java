package com.tataconsultancy.reto.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the OPERACIONES_TIPO_CAMBIO database table.
 * 
 */
@Entity
@Table(name="OPERACIONES_TIPO_CAMBIO")
@NamedQuery(name="OperacionesTipoCambio.findAll", query="SELECT o FROM OperacionesTipoCambio o")
public class OperacionesTipoCambio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_OPERACIONES_TIPO_CAMBIO", unique=true, nullable=false)
	private long idOperacionesTipoCambio;

	@Column(name="FECHA_REGISTRO", nullable=false)
	private Date fechaRegistro;

	@Column(name="MONTO_DESTINO", nullable=false, precision=10, scale=2)
	private BigDecimal montoDestino;

	@Column(name="MONTO_ORIGEN", nullable=false, precision=10, scale=2)
	private BigDecimal montoOrigen;

	//bi-directional many-to-one association to Cliente
	@ManyToOne
	@JoinColumn(name="ID_CLIENTE", nullable=false)
	private Cliente cliente;

	//bi-directional many-to-one association to TiposCambio
	@ManyToOne
	@JoinColumn(name="ID_TIPO_CAMBIO", nullable=false)
	private TiposCambio tiposCambio;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="ID_USUARIO_REGISTRA", nullable=false)
	private User usuario;

	public OperacionesTipoCambio() {
	}

	public long getIdOperacionesTipoCambio() {
		return this.idOperacionesTipoCambio;
	}

	public void setIdOperacionesTipoCambio(long idOperacionesTipoCambio) {
		this.idOperacionesTipoCambio = idOperacionesTipoCambio;
	}

	public Date getFechaRegistro() {
		return this.fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public BigDecimal getMontoDestino() {
		return this.montoDestino;
	}

	public void setMontoDestino(BigDecimal montoDestino) {
		this.montoDestino = montoDestino;
	}

	public BigDecimal getMontoOrigen() {
		return this.montoOrigen;
	}

	public void setMontoOrigen(BigDecimal montoOrigen) {
		this.montoOrigen = montoOrigen;
	}

	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public TiposCambio getTiposCambio() {
		return this.tiposCambio;
	}

	public void setTiposCambio(TiposCambio tiposCambio) {
		this.tiposCambio = tiposCambio;
	}

	public User getUsuario() {
		return this.usuario;
	}

	public void setUsuario(User usuario) {
		this.usuario = usuario;
	}

}