package com.tataconsultancy.reto.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the CLIENTES database table.
 * 
 */
@Entity
@Table(name="CLIENTES")
@NamedQuery(name="Cliente.findAll", query="SELECT c FROM Cliente c")
public class Cliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_CLIENTE", unique=true, nullable=false)
	private long idCliente;

	@Column(name="APELLIDOS", nullable=false, length=100)
	private String apellidos;

	@Column(name="DNI", nullable=false, length=100)
	private String dni;

	@Column(name="NOMBRES", nullable=false, length=100)
	private String nombres;

	//bi-directional many-to-one association to OperacionesTipoCambio
	@OneToMany(mappedBy="cliente")
	private List<OperacionesTipoCambio> operacionesTipoCambios;

	public Cliente() {
	}

	public long getIdCliente() {
		return this.idCliente;
	}

	public void setIdCliente(long idCliente) {
		this.idCliente = idCliente;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDni() {
		return this.dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombres() {
		return this.nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public List<OperacionesTipoCambio> getOperacionesTipoCambios() {
		return this.operacionesTipoCambios;
	}

	public void setOperacionesTipoCambios(List<OperacionesTipoCambio> operacionesTipoCambios) {
		this.operacionesTipoCambios = operacionesTipoCambios;
	}

	public OperacionesTipoCambio addOperacionesTipoCambio(OperacionesTipoCambio operacionesTipoCambio) {
		getOperacionesTipoCambios().add(operacionesTipoCambio);
		operacionesTipoCambio.setCliente(this);

		return operacionesTipoCambio;
	}

	public OperacionesTipoCambio removeOperacionesTipoCambio(OperacionesTipoCambio operacionesTipoCambio) {
		getOperacionesTipoCambios().remove(operacionesTipoCambio);
		operacionesTipoCambio.setCliente(null);

		return operacionesTipoCambio;
	}

}