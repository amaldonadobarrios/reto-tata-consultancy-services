package com.tataconsultancy.reto.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the MONEDA database table.
 * 
 */
@Entity
@Table(name="MONEDA")
@NamedQuery(name="Moneda.findAll", query="SELECT m FROM Moneda m")
public class Moneda implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_MONEDA", unique=true, nullable=false)
	private long idMoneda;

	@Column(name="NOMBRE_MONEDA", nullable=false, length=100)
	private String nombreMoneda;

	@Column(name="PAIS_MONEDA", nullable=false, length=50)
	private String paisMoneda;

	@Column(name="SIMBOLO_MONEDA", nullable=false, length=10)
	private String simboloMoneda;

	//bi-directional many-to-one association to TiposCambio
	@OneToMany(mappedBy="moneda1")
	private List<TiposCambio> tiposCambios1;

	//bi-directional many-to-one association to TiposCambio
	@OneToMany(mappedBy="moneda2")
	private List<TiposCambio> tiposCambios2;

	public Moneda() {
	}

	public long getIdMoneda() {
		return this.idMoneda;
	}

	public void setIdMoneda(long idMoneda) {
		this.idMoneda = idMoneda;
	}

	public String getNombreMoneda() {
		return this.nombreMoneda;
	}

	public void setNombreMoneda(String nombreMoneda) {
		this.nombreMoneda = nombreMoneda;
	}

	public String getPaisMoneda() {
		return this.paisMoneda;
	}

	public void setPaisMoneda(String paisMoneda) {
		this.paisMoneda = paisMoneda;
	}

	public String getSimboloMoneda() {
		return this.simboloMoneda;
	}

	public void setSimboloMoneda(String simboloMoneda) {
		this.simboloMoneda = simboloMoneda;
	}

	public List<TiposCambio> getTiposCambios1() {
		return this.tiposCambios1;
	}

	public void setTiposCambios1(List<TiposCambio> tiposCambios1) {
		this.tiposCambios1 = tiposCambios1;
	}

	public TiposCambio addTiposCambios1(TiposCambio tiposCambios1) {
		getTiposCambios1().add(tiposCambios1);
		tiposCambios1.setMoneda1(this);

		return tiposCambios1;
	}

	public TiposCambio removeTiposCambios1(TiposCambio tiposCambios1) {
		getTiposCambios1().remove(tiposCambios1);
		tiposCambios1.setMoneda1(null);

		return tiposCambios1;
	}

	public List<TiposCambio> getTiposCambios2() {
		return this.tiposCambios2;
	}

	public void setTiposCambios2(List<TiposCambio> tiposCambios2) {
		this.tiposCambios2 = tiposCambios2;
	}

	public TiposCambio addTiposCambios2(TiposCambio tiposCambios2) {
		getTiposCambios2().add(tiposCambios2);
		tiposCambios2.setMoneda2(this);

		return tiposCambios2;
	}

	public TiposCambio removeTiposCambios2(TiposCambio tiposCambios2) {
		getTiposCambios2().remove(tiposCambios2);
		tiposCambios2.setMoneda2(null);

		return tiposCambios2;
	}

}