package com.tataconsultancy.reto.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the OPCION_TIPO_CAMBIO database table.
 * 
 */
@Entity
@Table(name="OPCION_TIPO_CAMBIO")
@NamedQuery(name="OpcionTipoCambio.findAll", query="SELECT o FROM OpcionTipoCambio o")
public class OpcionTipoCambio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_OPCION_TIPO_CAMBIO", unique=true, nullable=false)
	private long idOpcionTipoCambio;

	@Column(name="NOMBRE_TIPO_OPCION", nullable=false, length=100)
	private String nombreTipoOpcion;

	//bi-directional many-to-one association to TiposCambio
	@OneToMany(mappedBy="opcionTipoCambio")
	private List<TiposCambio> tiposCambios;

	public OpcionTipoCambio() {
	}

	public long getIdOpcionTipoCambio() {
		return this.idOpcionTipoCambio;
	}

	public void setIdOpcionTipoCambio(long idOpcionTipoCambio) {
		this.idOpcionTipoCambio = idOpcionTipoCambio;
	}

	public String getNombreTipoOpcion() {
		return this.nombreTipoOpcion;
	}

	public void setNombreTipoOpcion(String nombreTipoOpcion) {
		this.nombreTipoOpcion = nombreTipoOpcion;
	}

	public List<TiposCambio> getTiposCambios() {
		return this.tiposCambios;
	}

	public void setTiposCambios(List<TiposCambio> tiposCambios) {
		this.tiposCambios = tiposCambios;
	}

	public TiposCambio addTiposCambio(TiposCambio tiposCambio) {
		getTiposCambios().add(tiposCambio);
		tiposCambio.setOpcionTipoCambio(this);

		return tiposCambio;
	}

	public TiposCambio removeTiposCambio(TiposCambio tiposCambio) {
		getTiposCambios().remove(tiposCambio);
		tiposCambio.setOpcionTipoCambio(null);

		return tiposCambio;
	}

}