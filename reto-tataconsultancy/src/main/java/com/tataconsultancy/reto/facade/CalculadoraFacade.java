package com.tataconsultancy.reto.facade;

import com.tataconsultancy.reto.beans.request.CalculadoraTipoCambioRequest;
import com.tataconsultancy.reto.beans.response.CalculadoraTipoCambioResponse;

public interface CalculadoraFacade {
	CalculadoraTipoCambioResponse calcularCambioDivisas(CalculadoraTipoCambioRequest request);
}
