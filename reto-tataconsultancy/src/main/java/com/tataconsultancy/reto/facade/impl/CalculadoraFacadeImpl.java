package com.tataconsultancy.reto.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tataconsultancy.reto.beans.request.CalculadoraTipoCambioRequest;
import com.tataconsultancy.reto.beans.response.CalculadoraTipoCambioResponse;
import com.tataconsultancy.reto.beans.response.TiposCambioResponse;
import com.tataconsultancy.reto.facade.CalculadoraFacade;
import com.tataconsultancy.reto.service.TiposCambioService;
@Component
public class CalculadoraFacadeImpl implements CalculadoraFacade {

	@Autowired
	TiposCambioService tiposCambioService;
	
	@Override
	public CalculadoraTipoCambioResponse calcularCambioDivisas(CalculadoraTipoCambioRequest request) {
		System.out.println(Long.valueOf(request.getIdMonedaOrigen()));
		TiposCambioResponse tipocambio=	tiposCambioService.findByMoneda1AndMoneda2(Long.valueOf(request.getIdMonedaOrigen()), Long.valueOf(request.getIdmonedaDestino()));
		return maptoTiposCambioResponse(request,tipocambio);
	}

	private CalculadoraTipoCambioResponse maptoTiposCambioResponse(CalculadoraTipoCambioRequest request, TiposCambioResponse tipocambio) {
		CalculadoraTipoCambioResponse response= new CalculadoraTipoCambioResponse();
		response.setMonedaOrigen(tipocambio.getMonedaOrigen());
		response.setMonedaDestino(tipocambio.getMonedaDestino());
		response.setMonto(request.getMonto());
		response.setMontoConTipoCambio(tipocambio.getOpcionTipoCambio().equals("COMPRA")?request.getMonto()/tipocambio.getValorCambio():request.getMonto()*tipocambio.getValorCambio());
		response.setTipoCambio(tipocambio.getOpcionTipoCambio());
		response.setValorTipoCambio(tipocambio.getValorCambio());
		return response;
	}
}
