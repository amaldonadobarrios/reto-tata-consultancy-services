package com.tataconsultancy.reto.security;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.tataconsultancy.reto.entity.Authority;
import com.tataconsultancy.reto.entity.User;

public final class JwtUserFactory {

  private JwtUserFactory() {
  }

  public static JwtUser create(User user) {
    return new JwtUser(
        user.getId(),
        user.getUsername(),
        user.getPassword(),
        user.getFirstname(),
        user.getLastname(),
        user.getEmail(),
        user.getEnabled(),
        user.getLastPasswordResetDate(),
        mapToGrantedAuthorities(user.getAuthorities()));
  }

  private static List<GrantedAuthority> mapToGrantedAuthorities(List<Authority> authorities) {
    return authorities.stream().map(authority -> new SimpleGrantedAuthority(authority.getName().name())).collect(Collectors.toList());
  }
}
