package com.tataconsultancy.reto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tataconsultancy.reto.entity.OpcionTipoCambio;


@Repository
public interface OpcionTipoCambioRepository extends JpaRepository<OpcionTipoCambio, Long> {
	

}
