package com.tataconsultancy.reto.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tataconsultancy.reto.entity.OperacionesTipoCambio;

public interface OperacionesTipoCambioRepository extends JpaRepository<OperacionesTipoCambio, Long>{

}
