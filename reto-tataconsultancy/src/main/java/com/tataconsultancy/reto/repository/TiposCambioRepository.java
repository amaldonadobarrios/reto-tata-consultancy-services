package com.tataconsultancy.reto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tataconsultancy.reto.entity.Moneda;
import com.tataconsultancy.reto.entity.TiposCambio;


@Repository
public interface TiposCambioRepository extends JpaRepository<TiposCambio, Long> {
	
	TiposCambio findByMoneda1AndMoneda2(Moneda monedaOrigen, Moneda monedaDestino);
}
