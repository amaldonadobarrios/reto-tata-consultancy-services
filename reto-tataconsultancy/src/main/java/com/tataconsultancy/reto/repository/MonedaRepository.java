package com.tataconsultancy.reto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tataconsultancy.reto.entity.Moneda;

@Repository
public interface MonedaRepository extends JpaRepository<Moneda, Long> {

}
