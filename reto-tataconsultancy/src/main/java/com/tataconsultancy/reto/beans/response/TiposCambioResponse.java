package com.tataconsultancy.reto.beans.response;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TiposCambioResponse {
	private long idTipoCambio;
	private Date fechaActualiza;
	private Date fechaRegistro;
	private String usuarioActualiza;
	private double valorCambio;
	private String monedaOrigen;
	private String monedaDestino;
	private String opcionTipoCambio;
	private String usuarioRegistra;
}
