package com.tataconsultancy.reto.beans.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CalculadoraTipoCambioResponse {

	private double monto;
	private String monedaOrigen;
	private String monedaDestino;
	private double montoConTipoCambio;
	private String tipoCambio;
	private double valorTipoCambio;
}
