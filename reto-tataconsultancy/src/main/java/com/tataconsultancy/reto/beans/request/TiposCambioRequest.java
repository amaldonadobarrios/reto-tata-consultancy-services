package com.tataconsultancy.reto.beans.request;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TiposCambioRequest {
	
	private double valorCambio;
	
	private Integer idMonedaOrigen;
	private Integer idMonedaDestino;
	
	@Max(value = 2)
	@Min(value = 1)
	private Integer idOpcionTipoCambio;
	
	private Integer idUsuario;
}
