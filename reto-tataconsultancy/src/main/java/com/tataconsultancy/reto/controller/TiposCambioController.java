package com.tataconsultancy.reto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tataconsultancy.reto.beans.request.TiposCambioRequest;
import com.tataconsultancy.reto.beans.response.TiposCambioResponse;
import com.tataconsultancy.reto.service.TiposCambioService;
@RestController
@RequestMapping("/tiposcambio")
public class TiposCambioController {

    @Autowired
	TiposCambioService tiposCambioService;

    @GetMapping("/listar")
    public List<TiposCambioResponse> findAll() {
        return tiposCambioService.findAll();
      }
    
    @GetMapping("/get/{id}")
    public TiposCambioResponse getTipoCambio(@PathVariable String id) {
        return tiposCambioService.get(Long.parseLong(id));
      }
	@PreAuthorize("hasRole('ROLE_ADMIN')") 
    @PostMapping("/save")
    public TiposCambioResponse save(@RequestBody TiposCambioRequest request) {
        return tiposCambioService.save(request);
      }
    
    @PutMapping("/edit/{id}")
    public TiposCambioResponse update(@PathVariable String id,  @RequestBody TiposCambioRequest request) {
        return tiposCambioService.update(Long.valueOf(id), request);
      }


}
