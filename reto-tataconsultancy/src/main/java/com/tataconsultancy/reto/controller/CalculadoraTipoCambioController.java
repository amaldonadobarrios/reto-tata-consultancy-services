package com.tataconsultancy.reto.controller;

import javax.validation.constraints.Positive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tataconsultancy.reto.beans.request.CalculadoraTipoCambioRequest;
import com.tataconsultancy.reto.beans.response.CalculadoraTipoCambioResponse;
import com.tataconsultancy.reto.facade.CalculadoraFacade;

@RestController
@RequestMapping("/calculadora")
public class CalculadoraTipoCambioController {

    @Autowired
	CalculadoraFacade calculadoraFacade;
    
    @PostMapping("/cambiodivisas")
    public CalculadoraTipoCambioResponse calcularCambioDivisas(@RequestBody CalculadoraTipoCambioRequest reqest) {
    	return calculadoraFacade.calcularCambioDivisas(reqest);
    }
}
