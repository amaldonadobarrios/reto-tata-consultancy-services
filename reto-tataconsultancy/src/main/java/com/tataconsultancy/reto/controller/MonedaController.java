package com.tataconsultancy.reto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tataconsultancy.reto.dto.MonedaDTO;
import com.tataconsultancy.reto.service.MonedaService;
@RestController
@RequestMapping("/moneda")
public class MonedaController {

    @Autowired
	MonedaService MonedaService;

    @GetMapping("/listar")
    public List<MonedaDTO> findAll() {
        return MonedaService.findAll();
      }
       


}
