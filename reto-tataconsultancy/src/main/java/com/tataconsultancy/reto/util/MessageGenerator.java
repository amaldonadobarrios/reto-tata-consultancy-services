package com.tataconsultancy.reto.util;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MessageGenerator {

    public Map<String, String> getMessage(int longitud) {
        int cont = 0;
        int maximum = 9;
        int minimum = 1;
        Map<String, String> mapMessage = new HashMap<String, String>();
        String code = "";
        String message = "";
        Date date = new Date();

        while (cont < longitud) {
            code = code.concat(String.valueOf((int) (Math.random() * (maximum - minimum)) + minimum));
            cont++;
        }

        mapMessage.put("code", code);
        //SimpleDateFormat format = new SimpleDateFormat("MM/dd hh:mm");
        message = MessageFormat.format(Constants.SMS_MESSAGE, code);
       // message = message.concat("(".concat(format.format(date)).concat(")"));
        mapMessage.put("code", code);
        mapMessage.put("message", message);

        return mapMessage;
    }
    
    static public String getNotificationMessage(String aTitulo, String aMessage) {
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("MM/dd hh:mm");
        String message = MessageFormat.format(Constants.SMS_NOTIFICATION_MESSAGE, aTitulo, aMessage);
        return message.concat("(".concat(format.format(date)).concat(")"));
    }

}
