package com.tataconsultancy.reto.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class FitbankException extends Exception {
	private static final long serialVersionUID = 1L;
	private String customCode;
	private String details;

	public FitbankException(String customCode, String message, String details) {
		super(message);
		this.customCode = customCode;
		this.details = details;
	}

	public String getCustomCode() {
		return customCode;
	}

	public void setCustomCode(String customCode) {
		this.customCode = customCode;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
	
	
}
