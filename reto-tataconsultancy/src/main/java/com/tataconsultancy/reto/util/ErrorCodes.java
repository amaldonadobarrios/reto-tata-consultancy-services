package com.tataconsultancy.reto.util;

import org.springframework.http.HttpStatus;


public enum ErrorCodes {
	ERROR_INTERNO_CORE(9000, "9000", "ERROR INTERNO EN CORE", HttpStatus.INTERNAL_SERVER_ERROR),
	ERROR_INTERNO_CORE_CONF_NO_IMPLEMENTADA(9000, "FIN067", "ERROR INTERNO DE CONFIGURACION DEL CORE",HttpStatus.INTERNAL_SERVER_ERROR, false),
	ERROR_GENERAL(9000, "9999", "ERROR GENERAL EN CORE ", HttpStatus.INTERNAL_SERVER_ERROR, false);

	private ErrorCodes(Integer value, String msg) {
		this.value = value;
		this.coreValue = value.toString();
		this.message = msg;
	}

	private ErrorCodes(Integer value, String coreValue, String msg) {
		this.value = value;
		this.coreValue = coreValue;
		this.message = msg;
		this.userMensajeCore = userMensajeCore;
	}

	private ErrorCodes(Integer value, String coreValue, String msg, Boolean userMensajeCore) {
		this.value = value;
		this.coreValue = coreValue;
		this.message = msg;
		this.userMensajeCore = userMensajeCore;
	}

	private ErrorCodes(Integer value, String coreValue, String msg, String detail) {
		this.value = value;
		this.message = msg;
		this.coreValue = coreValue;
		this.detail = msg;
		this.detail = detail;
	}

	private ErrorCodes(Integer value, String coreValue, String msg, String detail, HttpStatus status) {
		this.value = value;
		this.message = msg;
		this.coreValue = coreValue;
		this.detail = detail;
		this.httpStatusCode = status;
	}

	private ErrorCodes(Integer value, String coreValue, String msg, String detail, HttpStatus status,
			Boolean userMensajeCore) {
		this.value = value;
		this.message = msg;
		this.coreValue = coreValue;
		this.detail = detail;
		this.httpStatusCode = status;
		this.userMensajeCore = userMensajeCore;
	}

	private ErrorCodes(Integer value, String coreValue, String msg, HttpStatus status, Boolean userMensajeCore) {
		this.value = value;
		this.message = msg;
		this.coreValue = coreValue;
		this.detail = msg;
		this.httpStatusCode = status;
		this.userMensajeCore = userMensajeCore;
	}

	private ErrorCodes(Integer value, String coreValue, String msg, String detail, Boolean userMensajeCore) {
		this.value = value;
		this.message = msg;
		this.coreValue = coreValue;
		this.detail = detail;
		this.userMensajeCore = userMensajeCore;
	}

	private ErrorCodes(Integer value, String coreValue, String msg, HttpStatus status) {
		this.value = value;
		this.coreValue = coreValue;
		this.message = msg;
		this.detail = msg;
		this.httpStatusCode = status;
	}

	public static ErrorCodes findMessage(int value) {
		for (ErrorCodes v : ErrorCodes.values()) {
			if (v.value == value) {
				return v;
			}
		}
		return ERROR_GENERAL;
	}

	public static ErrorCodes findMessageByCoreCode(String value) {
		for (ErrorCodes v : ErrorCodes.values()) {
			if (v.coreValue.equalsIgnoreCase(value)) {
				return v;
			}
		}
		return ERROR_GENERAL;
	}

	Integer value;
	String coreValue;
	public String getCoreValue() {
		return coreValue;
	}

	public void setCoreValue(String coreValue) {
		this.coreValue = coreValue;
	}

	String message;
	String detail;
	HttpStatus httpStatusCode = HttpStatus.OK;
	Boolean userMensajeCore = true;

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Boolean getUserMensajeCore() {
		return userMensajeCore;
	}

}
