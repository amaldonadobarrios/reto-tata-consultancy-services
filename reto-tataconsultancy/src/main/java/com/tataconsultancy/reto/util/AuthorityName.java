package com.tataconsultancy.reto.util;

public enum AuthorityName {
  ROLE_USER, ROLE_ADMIN
}