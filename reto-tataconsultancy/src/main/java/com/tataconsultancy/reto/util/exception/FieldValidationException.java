package com.tataconsultancy.reto.util.exception;

import java.util.Set;

public class FieldValidationException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FieldValidationException(Set<String> keys) {
        super("Campo " + keys.toString() + " no  es válido");
    }

}