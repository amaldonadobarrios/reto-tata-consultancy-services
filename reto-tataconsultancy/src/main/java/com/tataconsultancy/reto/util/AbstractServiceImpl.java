package com.tataconsultancy.reto.util;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public abstract class AbstractServiceImpl<T extends Serializable, ID extends Serializable> {

  private JpaRepository<T, ID> repository;

  public void setRepository(JpaRepository<T, ID> repository) {
    this.repository = repository;
  }

  @Transactional(readOnly = true)
  public List<T> findAll() {
    return repository.findAll();
  }

  @Transactional(readOnly = true)
  public List<T> findAll(Sort sort) {
    return repository.findAll(sort);
  }

  @Transactional(readOnly = true)
  public List<T> findAllById(Iterable<ID> ids) {
    return repository.findAllById(ids);
  }

  @Transactional
  public <S extends T> List<S> saveAll(Iterable<S> entities) {
    return repository.saveAll(entities);
  }

  @Transactional
  public void flush() {
    repository.flush();
  }

  @Transactional
  public <S extends T> S saveAndFlush(S entity) {
    return repository.saveAndFlush(entity);
  }

  @Transactional
  public void deleteInBatch(Iterable<T> entities) {
    repository.deleteInBatch(entities);
  }

  @Transactional
  public void deleteAllInBatch() {
    repository.deleteAllInBatch();
  }

  @Transactional(readOnly = true)
  public T getOne(ID id) {
    return repository.getOne(id);
  }

  @Transactional(readOnly = true)
  public <S extends T> List<S> findAll(Example<S> example) {
    return repository.findAll(example);
  }

  @Transactional(readOnly = true)
  public <S extends T> List<S> findAll(Example<S> example, Sort sort) {
    return repository.findAll(example, sort);
  }
}
