package com.tataconsultancy.reto.util.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorEnum {
	ERROR_OPERACION("9991", "ERROR EN LA OPERACION"),
	ERROR_ASIGNACION_EXISTE("9992", "ERROR ASIGNACION YA EXISTE"),
	ERROR_ASIGNACION_NO_EXISTE("9993", "ASIGNACION NO EXISTE"),
	EXITO_OPERACION( "0000", "EXITO EN LA OPERACION"),
	
	//MENSAJE_OK("001", "Operacion realizada con exito"),
	//MENSAJE_ERROR("002", "Error en operacion"),
	//MENSAJE_EXCEPCION("003", "Error de excepcion del sistema"),
	//NO_DATA("004", "No se encontró datos"),
	//CODIGO_AUTO_PROYECTO_EXISTENTE("005", "Codigo de proyecto YA FUE VINCULADO A UN PROYECTO, generar uno nuevo -codigoProyecto/generar"),
	CODIGO_AUTO_PROYECTO_NO_EXISTENTE("006", "Codigo de proyecto NO EXISTE, generar uno nuevo -codigoProyecto/generar"),
	PARAMETROS_ERRADOS("007", "Parametro de entrada errado"),
	USUARIO_EXISTENTE("008", "Usuario ya existe"),
	ASIGNACION_ZONA_EXISTENTE("009", "Ubigeo ya esta asignado a una Zona"),
	ZONA_NO_HABILITADA("010", "Operacion no permitida, Zona esta desahabilitada"),
	RUC_EXISTENTE("011", "Operacion no permitida, Numero de Ruc está registrado"),
	ZONA_NO_EXISTE("012", "Operacion no permitida, Zona no existe"),
	ERROR_VALIDACION("013", "Error de validacion"),
	EMPRESA_SUPERVISORA_NO_ENROLADA("014", "El número de ruc  no pertenece a una Empresa Supervisora"),
	SUPERVISOR_SIN_EMPRESA("015","El perfil Supervisor debe vincularse a una empresa supervisora"),
	ESPECIALISTA_SIN_ZONA("016","El perfil Especialista debe vincularse a una Zona"),
	PARAMETRO_NULL("017","no puede ser Null o vacio"),
	REQUIERE_PN("018","solo se permite el persona Natural"),
	REQUIERE_PJ("019","solo se permite el persona Juridica"),
	PARAMETRO_DESAHABILITADO("020","no permitido o desactivado para el Perfil seleccionado"),
	DESTINO_SELECCIONADO_NO_DISPONIBLE("021", "Destino seleccionado no disponible");
	String codeError;
	String mensajeError;
}
