package com.tataconsultancy.reto.util;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;

public interface GenericService<T extends Serializable, ID extends Serializable> {

  List<T> findAll();

  List<T> findAll(Sort sort);

  List<T> findAllById(Iterable<ID> ids);

  <S extends T> List<S> saveAll(Iterable<S> entities);

  void flush();

  <S extends T> S saveAndFlush(S entity);

  void deleteInBatch(Iterable<T> entities);

  void deleteAllInBatch();

  T getOne(ID id);

  <S extends T> List<S> findAll(Example<S> example);

  <S extends T> List<S> findAll(Example<S> example, Sort sort);

}
