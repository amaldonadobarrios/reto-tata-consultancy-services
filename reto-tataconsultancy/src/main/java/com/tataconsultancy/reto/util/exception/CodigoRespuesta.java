package com.tataconsultancy.reto.util.exception;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class CodigoRespuesta {
	@ApiModelProperty(name = "Codigo", dataType = "String", example = "001")
	private String codigo;
	@ApiModelProperty(name = "descripcion", dataType = "String", example = "Operacion realizada con exito")
	private String descripcion;
}
