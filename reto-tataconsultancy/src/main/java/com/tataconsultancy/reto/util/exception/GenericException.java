package com.tataconsultancy.reto.util.exception;

public class GenericException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public GenericException(String  excepcion) {
        super(excepcion);
    }
}