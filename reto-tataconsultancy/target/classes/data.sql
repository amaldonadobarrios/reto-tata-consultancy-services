INSERT INTO `MONEDA` ( `ID_MONEDA`,`NOMBRE_MONEDA`,`SIMBOLO_MONEDA`,`PAIS_MONEDA`) VALUES (1,'SOL PERUANO', 'S./','PERU');
INSERT INTO `MONEDA` ( `ID_MONEDA`,`NOMBRE_MONEDA`,`SIMBOLO_MONEDA`,`PAIS_MONEDA`) VALUES (2,'DOLAR AMERICANO', '$./','EEUU');
INSERT INTO `MONEDA` ( `ID_MONEDA`,`NOMBRE_MONEDA`,`SIMBOLO_MONEDA`,`PAIS_MONEDA`) VALUES (3,'EURO', '€./','EUROPA');
INSERT INTO `MONEDA` ( `ID_MONEDA`,`NOMBRE_MONEDA`,`SIMBOLO_MONEDA`,`PAIS_MONEDA`) VALUES (4,'YUAN', '¥./','CHINA');
INSERT INTO `OPCION_TIPO_CAMBIO` ( `ID_OPCION_TIPO_CAMBIO`,`NOMBRE_TIPO_OPCION`) VALUES (1,'COMPRA');
INSERT INTO `OPCION_TIPO_CAMBIO` ( `ID_OPCION_TIPO_CAMBIO`,`NOMBRE_TIPO_OPCION`) VALUES (2,'VENTA');

INSERT INTO `CLIENTES` ( `ID_CLIENTE`,`APELLIDOS`,DNI,NOMBRES) VALUES (1,'PEREZ NAVARRO','44334433', 'LUIS');


insert into users (id, username, password, firstname, lastname, email, enabled, lastpasswordresetdate) values (1, 'user', '$2a$12$PSmrawXO08bL7L6usj7vn.hHhdbW/A1bXhOCRw5Z0IS9ZQQdgNaMa', 'user', 'user', 'user@user', 1, PARSEDATETIME('01-01-2023', 'dd-MM-yyyy'));
insert into users (id, username, password, firstname, lastname, email, enabled, lastpasswordresetdate) values (2, 'admin','$2a$12$/xyikf8Lm4TDlkU.F7Xa0up/.CAAfizHJ3e7UkNz5rHVGAOb6sIQe', 'admin', 'admin', 'admin@admin', 1, PARSEDATETIME('01-01-2023', 'dd-MM-yyyy'));

insert into authorities (id, name) values (1, 'ROLE_USER');
insert into authorities (id, name) values (2, 'ROLE_ADMIN');

insert into users_authorities (user_id, authority_id) values (1, 1);
insert into users_authorities (user_id, authority_id) values (2, 2);
INSERT INTO `TIPOS_CAMBIO` (ID_TIPO_CAMBIO,FECHA_REGISTRO,VALOR_CAMBIO,ID_MONEDA_ORIGEN,ID_MONEDA_DESTINO,ID_OPCION_TIPO_CAMBIO,ID_USUARIO_REGISTRA) VALUES 
(1,CURRENT_TIMESTAMP,'3.725',1,2,1,1);
INSERT INTO `TIPOS_CAMBIO` (ID_TIPO_CAMBIO,FECHA_REGISTRO,VALOR_CAMBIO,ID_MONEDA_ORIGEN,ID_MONEDA_DESTINO,ID_OPCION_TIPO_CAMBIO,ID_USUARIO_REGISTRA) VALUES 
(2,CURRENT_TIMESTAMP,'3.732',2,1,2,1);